# Emojify.py

**Superseded by [smoltext's](https://gitlab.com/imsofi/smoltext/) `smoltext --font emoji`**

---

```
$ emojify --help
Usage: emojify [OPTIONS] [TEXT]...

  Emojify: Simple to use string to emoji text converter.

  TEXT is the input text to be processed. Unless another input option is
  used, which will overwrite this argument.

Options:
  -c, --copy     Send output to clipboard.
  -p, --paste    Use clipboard as input.
  -v, --version  Show the version and exit.
  -h, --help     Show this message and exit.
$ emojify Hi!
:regional_indicator_h: :regional_indicator_i: :exclamation:
$ _
```

Simple to use string to emoji text converter designed for use with Discord.

### Current features:

* Processes text input and gives an output of emojis.
* Clipboard functionality to allow extra fast usage.
* Automaticcly splits long output for easy use.
* Has CLI commands built in with a nice help page.

### Feature wishlist:

* Easy installation with pip.
* File funcationality.

### Known issues:

* Discord is not able to handle word wrapping with the emojified output.

### Installation:

Download github repo with `git clone https://github.com/imsofi/emojify/`

Install the depencencies with `python3 -m pip install --user pyperclip click`

Run the `emojify.py` inside the downloaded folder with `python3 emojify.py`
